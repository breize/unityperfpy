'''
    calculators are used to perform calculations for metrics.
'''


class cpu():
    ''' calc percentBusy based on current and last interval values '''
    def __init__(self):
        ''' constructor '''
        self.oldvalues = {}

    def calc(self, invalue):
        ''' called every interval '''
        fields = invalue['fields']
        ihost = invalue['tags']['hostname']
        iname = invalue['tags']['name']

        index = ihost + ':' + iname
        if index in self.oldvalues:
            oldvalues = self.oldvalues[index]
            busyTicks = fields['busyTicks'] - oldvalues['busyTicks']
            idleTicks = fields['idleTicks'] - oldvalues['idleTicks']
            prctbusy = busyTicks / (busyTicks + idleTicks) * 100
            invalue['fields'] = {'percentBusy': round(prctbusy, 2)}

        self.oldvalues[index] = fields
        return True


# filesystem metrics are ugly. all values are absolute counters but to calculate
# latency the real read+write values are required.
# current values have to be stored as oldvalues and only starting with the second interval
# metrics produce meaningful values
class filesystem():
    ''' convert absolute values into relative rate values for filesystems '''
    def __init__(self):
        ''' constructor '''
        self.oldvalues = {}

    def calc(self, invalue):
        ''' called every interval '''
        fields = invalue['fields']
        ihost = invalue['tags']['hostname']
        iname = invalue['tags']['name']

        index = ihost + ':' + iname
        if index in self.oldvalues:
            oldvalues = self.oldvalues[index]
            self.oldvalues[index] = fields
            reads = int(fields['clientReads'] - oldvalues['clientReads'])
            readBytes = int(fields['clientReadBytes'] - oldvalues['clientReadBytes'])
            writes = int(fields['clientWrites'] - oldvalues['clientWrites'])
            writeBytes = int(fields['clientWriteBytes'] - oldvalues['clientWriteBytes'])
            if reads > 0:
                rLatency = round(fields['clientReadTime'] / reads, 2)
            else:
                rLatency = 0.0
            if writes > 0:
                wLatency = round(fields['clientWriteTime'] / writes, 2)
            else:
                wLatency = 0.0

            invalue['fields'] = {
                'clientReads': reads,
                'clientReadBytes': readBytes,
                'readLatency': rLatency,
                'clientWrites': writes,
                'clientWriteBytes': writeBytes,
                'writeLatency': wLatency,
                'iops': reads + writes,
                'mbps': readBytes + writeBytes,
                'clientReadTime': fields['clientReadTime'],
                'clientWriteTime': fields['clientWriteTime']
            }
        else:
            self.oldvalues[index] = fields
            return False
        return True


class disk():
    ''' calculate disk percent busy '''
    def calc(self, invalue):
        ''' called every interval '''
        fields = invalue['fields']
        prctbusy = fields['busyTicks'] / (fields['busyTicks'] + fields['idleTicks']) * 100
        invalue['fields']['busy'] = round(prctbusy, 2)
        return True
