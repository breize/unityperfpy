'''
Use a dedicated class as db writer. This makes it a lot easier to change the DB backend
Currently only prometheus is supported. InfluxDB was in the past but got removed to
avoid still having the import requirement
'''

import logging
import time
import platform
import sys

import prometheus_client as prom


class DbWriter():
    ''' abstract DbWriter() to support both influx and prometheus backends
        Only works with Prometheus now
    '''
    DB_BACKEND = 'prometheus'

    def __init__(self):
        self._port = 8080
        self._customtags = {}
        self._data = {}
        self._tagnames = []

    def Setup(self, port, tags={}):
        ''' constructor '''
        self._port = port
        self._customtags = tags
        self._tagnames = ['hostname', 'name'] + list(tags.keys())
        try:
            prom.start_http_server(self._port)
            self.intervaltimer = prom.Gauge('unity_collector_duration_seconds', '', self._tagnames)
            logging.info('Exposing data for Prometheus at port ' + str(self._port))
        except Exception as err:
            sys.exit('Error setting up Prometheus: ' + str(err))

    def WriteData(self, start, data):
        ''' expose data for prometheus '''
        for entry in data:
            mname = entry['measurement'].replace('.', '').lower()
            for k in entry['fields']:
                index = mname + ':' + k
                if index in self._data:
                    metric = self._data[index]
                else:
                    metric = prom.Gauge('unity_' + mname + '_' + k, '', self._tagnames)
                    self._data[index] = metric
                tags = self._customtags
                tags['hostname'] = entry['tags']['hostname']
                tags['name'] = entry['tags']['name']

                metric.labels(**tags).set_to_current_time()
                metric.labels(**tags).set(entry['fields'][k])

        # add collection interval duration metric
        tags = self._customtags
        tags['hostname'] = entry['tags']['hostname']
        tags['name'] = platform.node()
        self.intervaltimer.labels(**tags).set_to_current_time()
        self.intervaltimer.labels(**tags).set(time.time() - start)
        return
