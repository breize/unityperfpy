''' config class '''
import configparser
import os
import sys


class AppConfig():
    ''' hold the global configuration for an app '''

    CONFIG_FILE = 'unityperf.cfg'

    CFG_DEFAULTS = {
        'unity_ip': None,
        'unity_user': 'admin',
        'unity_password': None,
        'backend': 'prometheus',
        'backend_port': 8080,
        'interval': 60,
        'metrics': None,
        'tags': {},
    }
    CFG_INT_TYPE = ['backend_port', 'interval']
    CFG_DICT_TYPE = ['tags']
    CFG_LIST_TYPE = ['metrics']

    def __init__(self):
        ''' constructor, checking cfgfile and environment already
            cmdline args have to be called explicitely!
        '''
        self.cfg = AppConfig.CFG_DEFAULTS.copy()
        self._update_config_from_file()
        self._update_config_from_environment()
        self.check_config()

    def _update_config_from_file(self):
        ''' load configfile with configparser (section names are ignored) '''
        cfgparser = configparser.ConfigParser()
        try:
            cfgparser.read(AppConfig.CONFIG_FILE)
        except (IOError, OSError) as err:
            sys.exit(f'Uable to open {AppConfig.CONFIG_FILE}: {err.strerror}')
        for section in cfgparser.sections():
            for item in cfgparser[section]:
                if item in self.cfg:
                    self.cfg[item] = cfgparser[section][item]

    def _update_config_from_environment(self):
        ''' update config from environment, if found.
            ENV variables need to match config in UPPERCASE
        '''
        for key in self.cfg:
            varname = key.upper()
            if varname in os.environ:
                self.cfg[key] = os.environ[varname]

    def check_config(self):
        ''' validation for the config. int/list/dict types are handled '''
        for item in AppConfig.CFG_INT_TYPE:
            try:
                self.cfg[item] = int(self.cfg[item])
            except ValueError:
                sys.exit(f'{item} must be integer value')
        for item in AppConfig.CFG_DICT_TYPE:
            mydict = {}
            if not isinstance(self.cfg[item], str):
                continue
            for value in self.cfg[item].split(','):
                (key, val) = value.split('=')
                mydict[key] = val
            self.cfg[item] = mydict
        for item in AppConfig.CFG_LIST_TYPE:
            if not isinstance(self.cfg[item], str):
                continue
            self.cfg[item] = self.cfg[item].split(',')

    def update_config_from_args(self, args):
        ''' update config from commandline arguments '''
        for key in self.cfg:
            try:
                value = getattr(args, key)
                if value is not None:
                    self.cfg[key] = value
            except AttributeError:
                pass
        self.check_config()
