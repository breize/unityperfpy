''' metric class '''
import json
import os
import sys


class Metrics():
    ''' hold all metric definition and values '''

    METRICS_FILE = 'metricdef.json'

    def __init__(self):
        ''' load definition file already
            cmdline args have to be called explicitely!
        '''
        self.Paths = []
        self.Queries = []
        self.Capacitypaths = []
        self.Metricdef = {}
        self.Pathmapping = {}
        self._load_metricdef()

    def _load_metricdef(self):
        ''' load the metric definition file. Metrics are pre-parsed by the collector
            to decrease load on the backend/visualization layer
        '''
        if not os.path.isfile(Metrics.METRICS_FILE):
            sys.exit('File not found: ' + Metrics.METRICS_FILE)

        try:
            with open(Metrics.METRICS_FILE, 'r') as f:
                self.Metricdef = json.load(f)
        except IOError:
            sys.exit('Error loading {}: {}'.format(Metrics.METRICS_FILE, str(sys.exc_info()[1])))
        except json.JSONDecodeError:
            sys.exit('{} contains invalid json'.format(Metrics.METRICS_FILE))
        self._generate_pathmapping()

    def _generate_pathmapping(self):
        ''' create mapping for path names into metric definition (ease of access) '''
        for k in self.Metricdef:
            if self.Metricdef[k]['basepath'] in self.Pathmapping:
                sys.exit('Duplicate basepath detected: ' + self.Metricdef[k]['basepath'])
            self.Pathmapping[self.Metricdef[k]['basepath']] = k

    def UpdateInstances(self, selected, Unity):
        ''' load instances for one type/path from Unity '''
        metric = self.Metricdef[selected]
        if not metric['has_instances'] or not metric['dynamic_instances']:
            return
        # fields are id,name by default but some objects require others
        fields = 'id,name'
        idfield = 'id'
        if 'fields' in metric:
            fields = metric['fields']
            idfield = metric['idfield']
        typename = selected
        if 'typename' in metric:
            typename = metric['typename']
        metric['instances'] = Unity.get_type_instances(typename, fields, idfield)

    def AddMetricPath(self, selected):
        ''' generate Unity metric path names '''
        metricdef = self.Metricdef[selected]
        if 'capacity' in metricdef:
            self.Capacitypaths.append(selected)
            return
        # generate all metric paths
        for metric in metricdef['metrics']:
            path = 'sp.*.' + metricdef['basepath'] + '.'
            if metricdef['has_instances']:
                path += '*.'
            if 'is_kpi' in metricdef and metricdef['is_kpi'] is True:
                path = metricdef['basepath'] + '.'
                if metricdef['has_instances']:
                    path += '+.'
            self.Paths.append(path + metric)
