'''
Unity performance metric collection

REST API documentation: https://<IP>/apidocs/index.html
'''

import json
import logging
import sys
import requests
import urllib3
from requests.exceptions import HTTPError
urllib3.disable_warnings()


class UnityLib():
    ''' handle REST API calls for Unity '''

    METRIC_COLLECTIONS = [
        'blockCache',
        'cifs',
        'fibreChannel',
        'fs',
        'ftp',
        'http',
        'iscsi',
        'memory',
        'ndmp',
        'cpu',
        'net',
        'nfs',
        'ntp',
        'physical',
        'replication',
        'rpc',
        'ssh',
        'store',
        'storage',
        'vhdx',
        'virusChecker'
    ]

    METRIC_TYPES = {
        2: '32 bits counter',
        3: '64 bits counter',
        4: 'rate',
        5: 'fact',
        6: 'text',
        7: '32 bits virtual counter',
        8: '64 bits virtual counter'
    }
    # Unity has a user limitation of stat paths per query:
    # HTTP error 422: Too many stat paths are specified: [10]. Total stat path must be no more than [8]. Please reduce the number of stat paths. (Error Code:0x7d1410d)
    # This can be avoided by using the Visibilty:Engineering Header
    # MAX_PATHS_PER_QUERY = 8
    MAX_PATHS_PER_QUERY = 16

    # and another limit:
    # HTTP error 422: Unable to create any more queries. The system has reached the maximum number of active queries [5]. (Error Code:0x7d1400e)
    # MAX_ACTIVE_QUERY = 5
    MAX_ACTIVE_QUERY = 10

    def __init__(self):
        ''' Constructor '''
        self.host = None
        self.username = None
        self.password = None
        self.emc_csrf_token = None
        self.base_url = None
        self.headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Accept-Language': 'en-US',
            'X-EMC-REST-CLIENT': 'true',
            'Visibility': 'Engineering'  # from https://github.com/emc-openstack/storops
        }
        self.session = None

    # basic HTTP handling of a request
    def _request(self, r_type, path, payload=''):
        ''' handle the http request itself '''
        full_url = self.base_url + path
        logging.debug(full_url)
        try:
            if (r_type == 'GET'):
                response = self.session.get(full_url, headers=self.headers, verify=False)
            if (r_type == 'POST'):
                response = self.session.post(full_url, headers=self.headers, data=payload, verify=False)
        except HTTPError as http_err:
            sys.exit('Could not connect to ' + self.host + ': ' + str(http_err))
        except Exception as err:
            quit(str(err))
        logging.debug(response)
        return response

    # login/session handling
    def Connect(self, ipaddress, username, password):
        ''' perform login and get auth token '''
        self.host = ipaddress
        self.base_url = 'https://' + self.host + '/api/'
        self.username = username
        self.password = password
        if self.emc_csrf_token:
            return

        try:
            self.session = requests.Session()
            self.session.auth = (self.username, self.password)
            response = self.session.get(self.base_url + 'types/loginSessionInfo', headers=self.headers, verify=False)
            response.raise_for_status()
            logging.debug(response.json())
        except HTTPError as http_err:
            sys.exit('Could not connect to ' + self.host + ': ' + str(http_err))
        except Exception as err:
            sys.exit(str(err))
        self.headers['EMC-CSRF-TOKEN'] = response.headers['emc-csrf-token']
        self.collect_basicsysteminfo()
        logging.info('Connected to %s, %s, OE version %s', self.unity_hostname, self.unity_model, self.unity_version)

    def collect_basicsysteminfo(self):
        ''' get basic info from the system '''
        response = self.session.get(self.base_url + 'types/basicSystemInfo/instances', headers=self.headers, verify=False)
        resp = response.json()
        self.unity_hostname = resp['entries'][0]['content']['name']
        self.unity_model = resp['entries'][0]['content']['model']
        self.unity_version = resp['entries'][0]['content']['softwareVersion']

    def IsConnected(self):
        ''' Bool to check for connection status '''
        return (self.session is not None)

    def Disconnect(self):
        ''' nothing required for Unity, it will timeout '''
        pass

    def get(self, path):
        ''' HTTP GET request '''
        return self._request('GET', path).json()

    def post(self, path, payload):
        ''' HTTP POST request '''
        logging.debug(payload)
        return self._request('POST', path, payload).json()

    def delete(self, path):
        ''' HTTP DELETE request '''
        return self._request('DELETE', path).json()

    # get a dictionary of instances per type
    # the URL pattern is the same for all types, main challenge is that Unity
    # responses are split up in pages (2000 items per page default). For larger counts
    # all pages have to be concatenated together
    def get_type_instances(self, typename, fields, idfield='id'):
        ''' get instance list for a given type '''
        urlpath = 'types/' + typename + '/instances?compact=true&with_entrycount=true'
        resp = self.get(urlpath + '&fields=' + fields)
        if 'error' in resp and resp['error']['httpStatusCode'] != 200:
            err = resp['error']['httpStatusCode']
            msg = resp['error']['messages'][0]['en-US']
            print('Got HTTP error ' + str(err) + ' for type ' + typename + ': ' + msg)
            return None
        mylist = resp['entries']
        entryCount = resp['entryCount']
        page = 1
        while len(mylist) < entryCount:
            page = page + 1
            resp = self.get(urlpath + '&fields=' + fields + '&page=' + str(page))
            mylist = mylist + resp['entries']
        result = {}
        for item in mylist:
            result[item['content'][idfield]] = item['content']
        return result

    def get_metric_list(self, section):
        ''' get list of metrics per section '''
        if section not in self.METRIC_COLLECTIONS:
            quit('Path ' + section + ' is not a known collection type')
        response = self.get('types/metric/instances?compact=true&filter=path lk "sp.*.' + section + '.%25"')
        metrics = {}
        for k in response['entries']:
            i = k['content']['id']
            resp = self.get('instances/metric/' + str(i))
            content = resp['content']
            if content['isRealtimeAvailable']:
                metrics[i] = content
        return metrics

    def create_realtime_query(self, interval, paths):
        ''' create a realtim metric path request '''
        payload = {'interval': interval, 'paths': paths}
        resp = self.post('types/metricRealTimeQuery/instances', json.dumps(payload))
        if 'error' in resp and resp['error']['httpStatusCode'] != 200:
            logging.error(resp['error'])
            err = resp['error']['httpStatusCode']
            msg = resp['error']['messages'][0]['en-US']
            print('HTTP/' + str(err) + ': ' + msg)
            return None
        content = resp['content']
        return content['id']

    def query_metrics(self, query_id):
        ''' collect metrics '''
        result = self.get('types/metricQueryResult/instances?filter=queryId eq ' + str(query_id))
        logging.debug(result)
        return result['entries']
