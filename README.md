# UnityPerfPy
Python module to collect Unity performance metrics for [Grafana](https://www.grafana.com) with [Prometheus](https://prometheus.io/)

This library is influenced by [StorOps](https://github.com/emc-openstack/storops). Main difference is that storops implements the whole REST API, including create,modify,delete objects while this projects aims at just getting all performance metrics as easily as possible.

## Metrics
Metrics are organized in so called sets. See metricdef.json for details, new metrics will have to be added there. This whole definition allows to use Calculcators to convert the raw metrics into rate gauges and/or add sp-a and b data into one metrics.
The default collection interval is 1 minute.

## Configuration
Copy the example configuration file: ```cp example_unityperf.cfg unityperf.cfg```
And then edit the Unity connection details (IP, user, password) and adjust metrics as desired.

'''Tags''' allows to specify custom grouping tags. 'group' and 'location' are in the example but you can add any other tags as required. Please also add corresponding filter variables at the Grafana dashboard for them.

## Container (Docker)
Build the collector as container with: ```docker build -t unityperfpy .```
For starting it you can use the provided start_unityperfpy.sh script

## Prometheus
The collector can be added to an existing prometheus instance, just add a unity job to it:
```- job_name: unity
  honor_timestamps: true
  scrape_interval: 60s
  scrape_timeout: 60s
  metrics_path: /metrics
  scheme: http
  static_configs:
  - targets:
    - collectorhost:8080
```

To download and run a new prometheus instance: ```docker run prom/prometheus```

## Grafana
It is recommended to use your existing Grafana instance but to download and start a new one: ```docker run grafana/grafana```
Two example dashboards are provided, Unity File overview + details which can be imported.

