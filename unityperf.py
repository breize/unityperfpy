#!/usr/bin/env python3
#
import argparse
import logging
import signal
import sys
import time

from modules.config import AppConfig
from modules.dbwriter import DbWriter
from modules.unitylib import UnityLib
from modules.metrics import Metrics
from modules import calculators

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(message)s',
    datefmt='%d.%m.%Y-%H:%M:%S'
)

# GLOBALS
CFG = AppConfig()
DB = DbWriter()
Calculators = {}
Unity = UnityLib()
Metrics = Metrics()


def signal_handler(sig, frame):  # pylint: disable=unused-argument
    ''' handler for graceful exit '''
    logging.info('Stopping')
    if Unity.IsConnected():
        Unity.Disconnect()
    sys.exit(0)


def command_line_args():
    ''' parse command line args, if any '''
    parser = argparse.ArgumentParser(description='Collect Unity performance metrics')
    parser.add_argument('--unity_ip', help='unity system ip|host')
    parser.add_argument('--unity_user', help='unity system user')
    parser.add_argument('--unity_password', help='unity system password')
    parser.add_argument('--backend', help='backend type (influx|prometheus)')
    parser.add_argument('--backend_port', help='port to use for the backend')
    parser.add_argument('--interval', help='interval in seconds')
    parser.add_argument('--metrics', help='list of metrics (comma separated)')
    parser.add_argument('--tags', help='custom tags to use for grouping')
    return parser.parse_args()


def load_calculators():
    ''' load known calculator classes '''
    for k in Metrics.Metricdef:
        if 'calculator' in Metrics.Metricdef[k]:
            calcname = Metrics.Metricdef[k]['calculator']
            classdef = getattr(calculators, calcname)
            Calculators[calcname] = classdef()


def add_metric_fields(timestamp, measurement, name, metric, value, entries):
    ''' Unity reports all metrics as dedicated paths. Combine all metrics for the same
        object into fields for Grafana
    '''
    existing = False
    for entry in entries:
        if entry['measurement'] == measurement and entry['tags']['name'] == name:
            existing = True
            if metric in entry['fields']:
                entry['fields'][metric] += float(value)
            else:
                entry['fields'][metric] = float(value)
            break

    if existing is False:
        entry = {
            'measurement': measurement,
            'time': timestamp,
            'tags': {
                'hostname': Unity.unity_hostname,
                'name': name
            },
            'fields': {
                metric: float(value)
            }
        }
        entries.append(entry)
    return


def add_capacity_metrics(mname, metricdef, entries):
    ''' Capacity metrics are fields per instance, no calculations needed '''
    v = time.gmtime(time.time())
    timestamp = "{0:04}-{1:02}-{2:02}T{3:02}:{4:02}:{5:02}.000Z".format(
        v.tm_year, v.tm_mon, v.tm_mday, v.tm_hour, v.tm_min, v.tm_sec)
    for key in metricdef['instances']:
        i = metricdef['instances'][key]
        entry = {
            'measurement': mname,
            'time': timestamp,
            'tags': {
                'hostname': Unity.unity_hostname,
                'name': i['name']
            },
            'fields': {}
        }
        for f in metricdef['metrics']:
            entry['fields'][f] = i[f]
        entries.append(entry)


def process_metric(item, allentries):
    ''' process a metric
        return usable values from one metric
    '''
    item = item['content']
    path = item['path'].replace('sp.*.', '', 1)
    elems = path.split('.')
    metric = elems[len(elems) - 1]
    if elems[len(elems) - 2] == '*':
        basepath = '.'.join(elems[:-2])
    else:
        basepath = '.'.join(elems[:-1])
    measurement = Metrics.Pathmapping[basepath]
    conf = Metrics.Metricdef[measurement]
    timestamp = item['timestamp']
    values = item['values']

    # Unity reports metrics as spa->values and spb->values
    # where values can be a dictionary in case of type instances
    for sp in values.keys():
        if isinstance(values[sp], dict):
            for i in values[sp]:
                if conf['dynamic_instances']:
                    # Unity sometimes reports foreign objects.
                    # Like, for example for filesystem it also reports
                    # performance data for snapshots
                    # filter them out for now. If needed, they should just
                    # be added as a combined instance list
                    if i not in conf['instances']:
                        # Dynamic adding of new instances would be nice
                        # check if id matches the known pattern. Needed for filesystems since
                        # unity does report snapshots within the same metric
                        # TODO # skip this paragraph. Enough to refresh instances every hour or so
                        if 'idmatch' in conf and i[:len(conf['idmatch'])] == conf['idmatch']:
                            fields = 'id,name'
                            idfield = 'id'
                            if 'fields' in conf:
                                fields = conf['fields']
                                idfield = conf['idfield']
                            conf['instances'] = Unity.get_type_instances(measurement, fields, idfield)
                            if i not in conf['instances']:
                                continue
                        else:
                            continue
                    iname = conf['instances'][i]['name']
                elif not conf['combine_sp']:
                    iname = sp + '-' + i
                else:
                    iname = i
                add_metric_fields(timestamp, measurement, iname, metric, values[sp][i], allentries)
        else:
            add_metric_fields(timestamp, measurement, sp, metric, values[sp], allentries)


def setup_metric_queries():
    ''' Unity requires setup calls for metric paths '''
    for selected in CFG.cfg['metrics']:
        if selected not in Metrics.Metricdef:
            sys.exit('Section ' + selected + ' is not a known metric section')
        Metrics.UpdateInstances(selected, Unity)
        Metrics.AddMetricPath(selected)

    # Number of active queries is limited, unfortunately
    if len(Metrics.Paths) > (UnityLib.MAX_ACTIVE_QUERY * UnityLib.MAX_PATHS_PER_QUERY):
        sys.exit(str(len(Metrics.Paths)) + ' is too many paths! Unity is limited to max ' + str(UnityLib.MAX_ACTIVE_QUERY * UnityLib.MAX_PATHS_PER_QUERY) + ' stats queries at once')

    # create multiple queries depending on MAX_PATHS_PER_QUERY
    logging.info('Creating metric path queries for %d metrics', len(Metrics.Paths))
    num = len(Metrics.Paths)
    while num > 0:
        start = num - UnityLib.MAX_PATHS_PER_QUERY
        if start < 0:
            start = 0
        path_subset = Metrics.Paths[start:num]
        q_id = Unity.create_realtime_query(CFG.cfg['interval'], path_subset)
        if q_id is None:
            sys.exit('Critical error, could not create query. Aborting')
        Metrics.Queries.append(q_id)
        num = num - UnityLib.MAX_PATHS_PER_QUERY


def query_metrics(entries):
    for qid in Metrics.Queries:
        result = Unity.query_metrics(qid)
        for item in result:
            process_metric(item, entries)

    # apply calculators, if defined
    # reverse loop to allow deleting items inline
    i = len(entries) - 1
    while i >= 0:
        item = entries[i]
        mdef = Metrics.Metricdef[item['measurement']]
        if 'calculator' in mdef:
            clcalc = Calculators[mdef['calculator']]
            if clcalc.calc(item) is not True:
                del entries[i]
        i -= 1


def query_capacity_types(entries):
    ''' query capacity types '''
    for captype in Metrics.Capacitypaths:
        m = Metrics.Metricdef[captype]
        m['instances'] = Unity.get_type_instances(m['typename'], m['fields'], m['idfield'])
        add_capacity_metrics(captype, m, entries)


def query_health_status(entries):
    ''' add global system health status '''
    syshealth_dict = Unity.get_type_instances('system', 'id,health')
    for k in syshealth_dict.keys():
        syshealth = syshealth_dict[k]
    v = time.gmtime(time.time())
    timestamp = "{0:04}-{1:02}-{2:02}T{3:02}:{4:02}:{5:02}.000Z".format(
        v.tm_year, v.tm_mon, v.tm_mday, v.tm_hour, v.tm_min, v.tm_sec)
    entry = {
        'measurement': 'systemhealth',
        'time': timestamp,
        'tags': {
            'hostname': Unity.unity_hostname,
            'name': 'sys'
        },
        'fields': {
            'health': syshealth['health']['value']
        }
    }
    entries.append(entry)


def run_main_loop():
    ''' run the main application loop (gathering metrics) '''
    logging.info('Starting to monitor %d metrics', len(Metrics.Paths))
    while True:
        try:
            start = time.time()
            entries = []
            query_metrics(entries)
            query_capacity_types(entries)
            query_health_status(entries)
            DB.WriteData(start, entries)
            # log time taken
            delta = time.time() - start
            logging.info('Collected %i metrics in %2.3f seconds', len(entries), delta)
            time.sleep(float(CFG.cfg['interval'] - delta % CFG.cfg['interval']))
        except KeyboardInterrupt:
            print(' Stopping')


###################################################
# MAIN
###################################################
def main():
    ''' Application Main '''
    args = command_line_args()
    CFG.update_config_from_args(args)
    load_calculators()
    DB.Setup(CFG.cfg['backend_port'], CFG.cfg['tags'])

    # install signal handlers
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    # connect to Unity
    try:
        Unity.Connect(CFG.cfg['unity_ip'], CFG.cfg['unity_user'], CFG.cfg['unity_password'])
    except Exception as err:
        sys.exit('Cannot connect to Unity: ' + str(err))

    # setup queries and then start main loop
    setup_metric_queries()
    run_main_loop()


if __name__ == '__main__':
    main()
