#!/bin/bash
basedir=$(pwd)
port=8080
detach=true
cfgfile=$basedir/unityperf.cfg
#userid=487:487
#--user $userid \

docker run \
	--publish $port:8080 \
	--detach=$detach \
	--volume $cfgfile:/unityperfpy/unityperf.cfg \
	unityperfpy:latest
