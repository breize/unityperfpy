#
# Run the unityperfpy script inside a docker container
#
# Unity system and DB backend settings have to be provided via environment
# currently InfluxDB and Prometheus are supported as backends.
#
# Unity: UNITY_IP / UNITY_USER / UNITY_PASSWORD
# Backend: BACKEND / DATABASE_IP / DATABASE_PORT DATABASE_SCHEMA / DATABASE_USER / DATABASE_PASSWORD
# general: TAGS / METRICS / INTERVAL
#
FROM python:3-buster
COPY requirements.txt /
RUN pip install -r /requirements.txt
COPY . /unityperfpy
EXPOSE  8080
VOLUME [ /unityperfpy ]
WORKDIR /unityperfpy
ENTRYPOINT [ "/unityperfpy/unityperf.py" ]
