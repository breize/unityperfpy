## Makefile
pythonfiles = *.py modules/*.py

flake8: $(pythonfiles)
	flake8 --ignore=E501 $(pythonfiles)

pylint: $(pythonfiles)
	pylint --errors-only $(pythonfiles)

build: Dockerfile lint ## build docker container
	docker build -t unityperfpy .

.PHONY: lint test help

.DEFAULT_GOAL := help

lint: flake8 pylint ## code linting checks

test: lint ## run code tests

help: ## this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "%-30s %s\n", $$1, $$2}'

